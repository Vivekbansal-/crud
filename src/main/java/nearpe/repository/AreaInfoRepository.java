package nearpe.repository;


import nearpe.model.AreaInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;


public interface AreaInfoRepository extends PagingAndSortingRepository<AreaInfo, Long>,
        JpaSpecificationExecutor<AreaInfo> {
}