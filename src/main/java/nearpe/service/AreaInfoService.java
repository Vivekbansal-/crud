package nearpe.service;

import nearpe.model.AreaInfo;
import nearpe.repository.AreaInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class AreaInfoService {


    @Autowired
    private AreaInfoRepository areaInfoRepository;

    private boolean existsById(Long id) {
        return areaInfoRepository.existsById(id);
    }


public AreaInfo adddata(AreaInfo info)  {

       AreaInfo result= this.areaInfoRepository.save(info);
       return result;

}

    public void updateRecord(AreaInfo areaInfo,long id) {

        areaInfo.setId(id);
        areaInfoRepository.save(areaInfo);
    }

    public List<AreaInfo> findAll(){
        List<AreaInfo> bes = (List<AreaInfo>)areaInfoRepository.findAll();
        return bes;
    }


    public AreaInfo findById(Long id)  {
        AreaInfo area = areaInfoRepository.findById(id).orElse(null);
        return area;
    }


    public void deleteById(long id) {
            areaInfoRepository.deleteById(id);
    }

}