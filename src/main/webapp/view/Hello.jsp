<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<style><%@include file="/stylesheet/style.css"%></style>


<body>
    <div align="center">
        <table border="1">
          <thead>
            <tr>
             <td>id</td>
             <td>zip</td>
             <td>city</td>
             <td>state</td>
             <td>district</td>
            </tr>
          </thead>

        <c:forEach var="task" items="${tasks}">
        <tr>
          <td>${task.id}</td>
          <td> ${task.zip}</td>
          <td> ${task.city}</td>
          <td> ${task.state}</td>
          <td> ${task.district}</td>
        </tr>
        </c:forEach>
        </table>
    </div>
</body>
</head>
</html>